$(function(){
  if(!$('body').hasClass('admin-innoawards-products')) return;

  // 参选按钮
  function listenChoiceModal(){
    var $choosen = $('.choosen');

    $choosen.on('click', function(){
      var $choosenModal = $('#choosen-modal');
      $choosenModal.find('#rank_product_id').val($(this).data('pro-id'));
      $choosenModal.modal('show');
    });
  };

  listenChoiceModal();

  // 参选modal提交
  var $rankSubmit = $('#rankSubmit');

  $rankSubmit.on('click', function(){
    $('#rankForm').submit();
  });
});
