module V1
class API < Grape::API
  include APIGuard
  version 'v1', using: :path
  format :json
  content_type :json, 'application/json'
  formatter :json, Grape::Formatter::Jbuilder

  mount ProductAs
  mount Award

  add_swagger_documentation base_path: 'api', api_version: 'v1', mount_path: 'new_api'
end
end
