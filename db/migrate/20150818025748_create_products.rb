class CreateProducts < ActiveRecord::Migration
  def change
    create_table :products, id: :uuid, default: 'uuid_generate_v4()' do |t|
      # product normal field
      t.string :logo
      t.string :screenshots, array: true
      t.string :name, null: false, index: true
      t.string :summary
      t.text :description
      t.date :published_at
      t.string :url

      # product weibo wechat id field
      t.string :weibo
      t.string :wechat

      # product category field
      t.string :category, index: true

      # recommended platform
      t.string :recommended_platform, index: true

      # contact information
      t.string :c_name, null: false, index: true
      t.string :c_corp
      t.string :c_position
      t.string :c_email
      t.string :c_mobile
      t.string :c_corp_addr

      # editor evaluation
      t.text :evaluation

      # product status such as unapproved approved
      t.string :status, null: false, index: true

      t.timestamps null: false
    end
  end
end
