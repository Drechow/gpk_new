GrapeSwaggerRails.options.url      = '/api/v1/new_api'
GrapeSwaggerRails.options.app_url  = ''
GrapeSwaggerRails.options.app_name = 'GpkNew'
GrapeSwaggerRails.options.api_key_name = 'access_token'
GrapeSwaggerRails.options.before_filter do |request|
  user = User.find_by_id session[:user_id]
  user && user.admin? || redirect_to(request.protocol + request.host_with_port)
end
