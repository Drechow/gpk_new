//= require ./awards
//= require ./innoawards
//= require ./innoawards/partners
//= require ./innoawards/products
//= require ./partners
//= require ./products
//= require ./product_as

$(function() {
  $('.ui.checkbox').checkbox();

  $('.popup-title').popup();

  $('.accordion').accordion();
});
