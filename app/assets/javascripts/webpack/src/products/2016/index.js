import $ from 'jquery';
// import modernizr from 'modernizr';

import goTop from '../../shared/go_top';
import productsShow from './products_show';

import productsNew from '../products_new';

$(function () {
  goTop();
  if($('body').hasClass('products_show')) {
    productsShow();
  }
  if($('body').hasClass('products_new')) {
    productsNew();
  }
});
