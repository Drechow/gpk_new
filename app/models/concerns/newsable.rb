module Newsable
  extend ActiveSupport::Concern

  included do
    has_many :news, class_name: 'News', as: :newsable
  end
end
