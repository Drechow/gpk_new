require 'rails_helper'

RSpec.describe Innoaward, type: :model do
  let(:innoaward) { create(:innoaward) }

  describe '#save' do
    context 'when valid' do
      it { expect(innoaward).to be_valid }
    end

    context 'when without title' do
      it do
        innoaward.title = ''
        expect(innoaward).not_to be_valid
      end
    end

    context 'when without year' do
      it do
        innoaward.year = nil
        expect(innoaward).not_to be_valid
      end
    end
  end
end
