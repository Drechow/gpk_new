import $ from 'jquery';
import { each } from 'lodash';

/*
  图片上传预览，传入参数自动处理文件预览及文件删除

  @limit 限制几个图片上传
  @template 输入框的模板，必须是 <div><input /></divj> 有一层标签包裹input
  @previewArea 预览 DOM 放在哪
  @previewClass 预览的类 区分不同样式
  @addButtonClass 添加图片按钮的类

  F.clear 清空所有的预览及隐藏的 input
*/

let preview = ({limit, template, previewArea, previewClass, addButtonClass}) => {
  const $template = $(template).html();

  // sum 是一个计数器，防止超过限制
  let sum = 0;

  let clear = () => {$(previewArea.empty())};

  // 核心，用于新增一个上传图片的按钮
  let addBtn = () => {
    $(previewArea)
      .append($template)
      .find('input[type=file]').on('change', addFile);
  };

  addBtn();

  // 当文件被选择时转成预览，并根据需要决定是否显示上传
  function addFile(e) {
    const $input = $(e.target);
    const $parent = $input.parent();
    const $addBtn = $parent.clone();

    readFile(e.target.files, data => {
      $input.attr('name', $input.data('name'));
      $parent
        .addClass(previewClass)
        .removeClass(addButtonClass)
        .css('background-image', `url(${data})`)
        .on('click', removePreview);

      sum ++;
      $input.hide();
      if(sum < limit) {
        addBtn();
      }
    });
  }

  // 删除预览
  function removePreview(e) {
    if(sum >= limit) {
      addBtn();
    }

    sum --;
    $(e.target).remove();
  }

  return { clear };
};

// 提供一个文件读取函数，每次读取到内容时执行回调，支持多文件
function readFile(files, cb) {
  each(files, file =>  {
    let reader = new FileReader();
    reader.onload = e => {
      cb(e.target.result);
    };
    reader.readAsDataURL(file);
  });
}

export default preview;
