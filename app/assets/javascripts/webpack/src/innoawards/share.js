let config = {
  title: '今年最棒的50款产品都在这儿了，为TA们投上你宝贵的一票吧！',
  desc: '让最好的产品获得最高的荣誉！由极客公园发起的年度中国互联网创新产品评选（InnoAwards）已经开始啦，如果你认为自己拥有创新的力量，那就速速报上名来~',
  link: 'http://innoawards.geekpark.net/',
  imgUrl: gon.shareIMG || 'https://o185dmv7p.qnssl.com/innoawardsshare-icon.jpg'
};

export default { config };
