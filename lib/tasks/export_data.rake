namespace :export_data do
  require 'csv'

  desc 'export products data to csv'
  task :products => :environment do |args|
    column_names = ['name', 'url', 'summary', 'description', 'published_at', 'weibo', 'category', 'recommended_platform', 'c_name', 'c_corp', 'c_position', 'c_email', 'c_mobile', 'c_corp_addr', 'evaluation', 'status']
    csv = CSV.open(Rails.root.join('export_products.csv'), 'w') do |csv|
      csv << column_names
      products = Product.all
      products.each do |pro|
        csv << column_names.map{|col| pro.public_send(col)}
      end
    end
  end
end
