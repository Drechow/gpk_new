# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rake db:seed (or created alongside the db with db:setup).
#
# Examples:
#
#   cities = City.create([{ name: 'Chicago' }, { name: 'Copenhagen' }])
#   Mayor.create(name: 'Emanuel', city: cities.first)
50.times do |n|
  product = Product.create({name: "product_#{n}", summary: "summary_#{n}", description: "description_#{n}",
                            url: "http://example.com/product_#{n}",
                            logo: File.open('./app/assets/images/default_logo.jpg'),
                            screenshots: [File.open('./app/assets/images/default_logo.jpg')],
                            published_at: '2015-09-01', category: 'internet_hardware',
                            recommended_platform: '创新工场', c_name: "c_name_#{n}",
                            c_corp: "c_corp_#{n}", c_position: "c_pos_#{n}",
                            c_email: "c_email_#{n}@example.com", c_mobile: "13012345678",
                            c_corp_addr: "some where"})
end

10.times do |i|
  inno = Innoaward.create({year: "20#{i+10}", title: "Innoaward_#{i}"})
  4.times do |n|
    news = News.create({title: "news_#{i}_#{n}", url: "news_#{i}_#{n}", newsable_id: inno.id, newsable_type: 'Innoaward'})
    award = Award.create({name: "award_#{n}", summary: "summary_#{n}", description: "description_#{n}", innoaward_id: inno.id})
    5.times do |j|
      rank = Rank.create({innoaward_id: inno.id, product_id: Product.order(created_at: :desc).first(rand(49) + 1).last.id, award_id: award.id})
    end
  end
end
