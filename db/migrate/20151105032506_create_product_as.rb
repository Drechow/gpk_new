class CreateProductAs < ActiveRecord::Migration
  def change
    create_table :product_as, id: :uuid, default: 'uuid_generate_v4()' do |t|
      # product nomal field
      t.string :logo
      t.string :screenshots, array: true
      t.string :name, null: false, index: true
      t.string :summary
      t.text :description
      t.date :published_at
      t.string :url
      t.string :category, index: true
      t.uuid :user_id, index: true

      # contact field
      t.jsonb :contacts

      # founder field
      t.jsonb :founders

      # financing field
      t.jsonb :financings

      # cowork record
      t.jsonb :cowork_records

      # editor comments
      t.jsonb :editor_comments

      # import form innoawards
      t.uuid :innoaward_id

      t.timestamps null: false
    end
  end
end
