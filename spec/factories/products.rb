FactoryGirl.define do
  factory :product do
    sequence :name do |n|
      "test_product_#{n}"
    end
    summary '测试产品的简述'
    description '测试产品的描述 等等 等等'
  end
end
