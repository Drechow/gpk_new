class Image < ActiveRecord::Base
  mount_uploader :file, ImageUploader
  belongs_to :imageable, polymorphic: true, touch: true
  validates :file, presence: true
  default_scope { order(created_at: :asc) }
end
