class InnoawardsController < ApplicationController
  layout :enable_layout

  def index
    current_year = available_years.include?(params[:year]) ? params[:year] : available_years.last
    @innoaward = Innoaward.find_by_year(current_year.to_i)
    gon.awards = @innoaward.get_ranks
    @history_review = @innoaward.extra_info["history_review"].reverse
    @partners = @innoaward.get_partners_json
    @topics = get_activity_reports
    @awards = @innoaward.awards.rank(:row_order)
    render "innoawards/#{current_year}/index.html.slim"
  end

  def upbrowser
    render 'shared/upbrowser', layout: false
  end

  private

  def get_activity_reports
    uri = URI(URI.encode('http://old.geekpark.net/api/v1/topics/get_collection.json?name=InnoAwards'))
    result = JSON.parse(Net::HTTP.get(uri))
    result.is_a?(Hash) ? [] : result.first(6)
  end
end
