import $ from 'jquery';
import modernizr from 'modernizr';

import goTop from '../shared/go_top';
import showPageSlider from './show_page_slider';

import productsNew from '../products/products_new';

$(function () {
  goTop();
  if($('body').hasClass('products_show')) {
    showPageSlider();
  }
  if($('body').hasClass('products_new')) {
    productsNew();
  }
});
