RSpec.configure do |config|
  # 加入FactoryGirl辅助方法，如果不加这一行，调用FactoryGirl时需要在方法前加 FactoryGirl
  config.include FactoryGirl::Syntax::Methods
  # Linting Factories，factory在build时会调用 valid？ 方法，如果返回false，会报错，保证factory的正确性
  # DatabaseCleaner，每次测试后清理数据库
  config.before(:suite) do
    begin
      DatabaseCleaner.start
      FactoryGirl.lint
    ensure
      DatabaseCleaner.clean
    end
  end
end
