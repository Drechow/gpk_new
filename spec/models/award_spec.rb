require 'rails_helper'

RSpec.describe Award, type: :model do
  let(:award){ create(:award) }

  describe '#save' do
    context 'when valid' do
      it { expect(award).to be_valid }
    end

    context 'without name' do
      it do
        award.name = ''
        expect(award).not_to be_valid
      end
    end
  end
end
