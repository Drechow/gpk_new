require 'rails_helper'

RSpec.describe Admin::RanksController, type: :controller do

  let(:inno) { create(:innoaward) }

  describe '#index' do
    it 'render index' do
      get :index, innoaward_id: inno.id
      expect(response).to render_template(:index)
    end
  end

  describe '#create' do
    let(:product) { create(:product) }
    let(:award) { create(:award) }
    it 'when valid' do
      count_before = inno.ranks.count
      post :create, {rank: {product_id: product.id, award_id: award.id}}.merge({innoaward_id: inno.id})
      expect(inno.ranks.count).to eq(count_before + 1)
    end
  end

  describe '#destroy' do
    let!(:rank) { create(:rank, innoaward: inno) }
    it 'destroy item' do
      count_before = inno.ranks.count
      delete :destroy, innoaward_id: inno.id, id: rank.id
      expect(inno.ranks.count).to eq(count_before - 1)
    end
  end

end
