OmniAuth.config.path_prefix = '/users/auth'

Rails.application.config.middleware.use OmniAuth::Builder do
  provider :geekpark, ENV['GEEKPARK_KEY'], ENV['GEEKPARK_SECRET'], scope: 'public admin'
end
