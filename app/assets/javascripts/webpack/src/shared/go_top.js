import $ from 'jquery';

function goTop() {
  const $share = $('#share-gotop');
  const $document = $(document);

  $document.scroll(function () {
    if($document.scrollTop() > 200) {
      $share.addClass('show');
    } else {
      $share.removeClass('show');
    }
  });

  $('#go-top').on('click', function () {
    $('html,body').animate({
      scrollTop: 0 + 'px'
    }, 800);
  });
}

export default goTop;
