创新产品评选
---------

### 前端结构
1. 打包 JS 使用 webpack，将打包好的文件放在 `app/assets/javascripts/webpack/dist` 目录下
2. webpack 配置文件在 `app/assets/javascripts/webpack` 目录下
3. 所有的 node_modules 也在 webpack 目录下
