class Admin::AdminController < ApplicationController
  # Prevent CSRF attacks by raising an exception.
  # For APIs, you may want to use :null_session instead.
  protect_from_forgery with: :exception
  layout 'admin/layouts/admin'

  before_action :require_login #, :require_staff unless Rails.env.development?
end
