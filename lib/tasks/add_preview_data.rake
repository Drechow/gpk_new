namespace :add_preview_data do
  task :add_preview => :environment do |args|
    inno = Innoaward.find_by_year 2016
    products = []
    inno.awards.rank(:row_order).each do |a|
      a.ranks.where(order: 1).each do |rank|
        product = rank.try(:product)
        products << {
          'type' => "年度#{a.name}",
          'name' => product.name,
          'logo' => "https://o185ynvh1.qnssl.com/#{product.logo.url}"
        }
      end
    end
    result = {
      'year' => 2016,
      'products' => products
    }
    previous_data = inno.extra_info["history_review"]
    new_data = previous_data.push result
    new_inno = Innoaward.find_by_year 2016
    new_inno.extra_info["history_review"] = new_data
    new_inno.save
  end
end

