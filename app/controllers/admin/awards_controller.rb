class Admin::AwardsController < Admin::AdminController
  before_action :find_innoaward, only: [:index, :new, :create]

  def index
    @awards = @innoaward.awards.rank(:row_order).page params[:page]
  end

  def change_order
    @award = Award.find(params[:id])
    @award.update(row_order_position: params[:method_name]) if
      permit_action.include? params[:method_name]
    redirect_to admin_innoaward_awards_path(@award.innoaward)
  end

  def show
    @award = Award.find(params[:id])
  end

  def new
    @innoawards = Innoaward.all
    # @innoaward = Innoaward.find_by_year(Date.today.year)
    # 暂时写死
    @innoaward = Innoaward.find('7ee025ee-9f39-483a-b0f0-4586e4a7264a')
    @award = Award.new
  end

  def create
    @award = Award.new(award_params)
    if @award.save
      redirect_to admin_innoaward_path(@award.innoaward)
    else
      render :new
    end
  end

  def edit
    @award = Award.find(params[:id])
    @innoawards = Innoaward.all
    @innoaward = @award.innoaward
  end

  def update
    @award = Award.find(params[:id])
    if @award.update(award_params)
      redirect_to admin_innoaward_awards_path(@award.innoaward)
    else
      render :edit
    end
  end

  def destroy
    @award = Award.find(params[:id])
    @award.destroy
    redirect_to admin_innoaward_awards_path(@award.innoaward)
  end

  private
  def award_params
    params.require(:award).permit(:name, :summary, :description, :innoaward_id)
  end

  def find_innoaward
    @innoaward = Innoaward.find(params[:innoaward_id])
  end

  def permit_action
    ['up', 'down']
  end
end
