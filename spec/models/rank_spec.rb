require 'rails_helper'

RSpec.describe Rank, type: :model do
  let(:rank) { create(:rank) }

  describe '#save' do
    context 'when valid' do
      it { expect(rank).to be_valid }
    end

    context 'without product_id' do
      it do
        rank.product_id = nil
        expect(rank).not_to be_valid
      end
    end

    context 'without award_id' do
      it do
        rank.award_id = nil
        expect(rank).not_to be_valid
      end
    end
  end
end
