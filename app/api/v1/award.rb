class V1::Award < Grape::API
  resources :innoawards do
    namespace ':innoaward_id' do
      resources :awards do
        desc '获得指定年份的奖项'
        get jbuilder: 'award' do
          @innoaward = Innoaward.find(params[:innoaward_id])
          @awards = @innoaward.awards.rank(:row_order)
        end
      end
    end
  end
end
