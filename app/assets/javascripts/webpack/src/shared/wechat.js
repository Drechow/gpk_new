import $ from 'jquery';

const wechat = {
  init: function(){
    let sign_package = gon.sign_package;
    const share = {
      title: '好产品都到碗里来！报名2015 InnoAwards创新产品评选，让大家一起为你点赞！',
      desc: '让最好的产品获得最高的荣誉！由极客公园发起的年度中国互联网创新产品评选（InnoAwards）已经开始啦，如果你认为自己拥有创新的力量，那就速速报上名来~',
      link: 'http://innoawards.geekpark.net/',
      imgUrl: 'https://o185dmv7p.qnssl.com/innoawardsshare-icon.jpg'
    };

    wx.config({
      debug: false,
      appId: sign_package.appId,
      timestamp: sign_package.timestamp,
      nonceStr: sign_package.nonceStr,
      signature: sign_package.signature,
      jsApiList: [
        'onMenuShareTimeline',
        'onMenuShareAppMessage',
        'onMenuShareQQ',
        'onMenuShareWeibo'
      ]
    });
    wx.ready(function(){
      wx.onMenuShareTimeline(share);
      wx.onMenuShareQQ(share);
      wx.onMenuShareAppMessage(share);
      wx.onMenuShareQZone(share);
    });
    wx.error(function(res){
      alert(JSON.stringify(res));
    });
  }
};

export default wechat;
