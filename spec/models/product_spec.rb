require 'rails_helper'

RSpec.describe Product, type: :model do
  let(:product) { create(:product) }

  describe '#save' do
    context 'when valid' do
      it { expect(product).to be_valid }
    end

    context 'without name' do
      it do
        product.name = ''
        expect(product).not_to be_valid
      end
    end

    context 'without summary' do
      it do
        product.summary = ''
        expect(product).not_to be_valid
      end
    end
  end
end
