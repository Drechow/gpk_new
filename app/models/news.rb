class News < ActiveRecord::Base
  mount_uploader :cover, CoverUploader

  belongs_to :newsable, polymorphic: true

  validates :title, presence: true
  validates :title, uniqueness: true

  validates :url, format: { with: /\A(https?:\/\/)?([\da-z\.-]+)\.([a-z\.]{2,6})([\/\w \.-]*)*\/?\z/ }
end
