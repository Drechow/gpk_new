import $ from 'jquery';
import { render } from 'react-dom';
import React from 'react';
import ShareFixed from '@geekpark/react-share-fixed';
import scrollTo from '@geekpark/scroll-to';
import { isMobileView, isMobile } from 'mdetect';
$(function () {

  if(!$('body').hasClass('innoawards_index')) return;
  /* agendaSwiper */
  const $navItem = $('.js-agenda-nav').find('a.item');
  const agendaSwiper = new Swiper ('#agenda-list-container', {
    autoHeight: isMobileView(),
    initialSlide: 3,
    direction: isMobileView() ? 'horizontal' : 'vertical',
    loop: false,
    simulateTouch: false,
    onSlideChangeStart: s => {
      const index = s.activeIndex;
      const $item = $navItem.eq(index);
      switchToItem($item);
    }
  });

  $navItem.on('click', function() {
    const $this = $(this);
    switchToItem($this);
    agendaSwiper.slideTo($this.index());
  });

  /* pressSwiper */
  const pressSwiper = new Swiper('#press-container', {
    direction: 'horizontal',
    loop: false,
    autoHeight: true,
    nextButton: '#press-next',
    prevButton: '#press-prev',
  });


  /* auditSwiper */
  // const auditSwiper = new Swiper('#audit-container', {
  //   direction: isMobileView() ? 'vertical' : 'horizontal',
  //   loop: false,
  //   nextButton: isMobileView() ? 'null' : '#audit-next',
  //   prevButton: isMobileView() ? 'null' : '#audit-prev',
  //   pagination: isMobileView() ? 'null' : '#audit-pagination',
  //   paginationClickable: true,
  // });

  /* ceremonySwiper */
  const ceremonySwiper = new Swiper('#ceremony-container', {
    direction: isMobileView() ? 'vertical' : 'horizontal',
    effect: isMobileView() ? 'slide' :'fade',
    initialSlide: 0,
    fade: {
      crossFade: true,
    },
    simulateTouch: false,
    onlyExternal: true,
    pagination: '#ceremony-pagination',
    paginationClickable: true,
    paginationBulletRender: function (index, className) {
      return `<h4 class="${className}">${(2016 - index)}</h4>`;
    }
  });

  // Vote Swiper for mobile
  const $awardItem = $('.js-award-wrapper').find('a.item');
  const voteSwiper = new Swiper('#vote-container', {
    direction: 'horizontal',
    loop: false,
    effect: 'fade',
    fade: {
      crossFade: true,
    },
    autoHeight: true,
    simulateTouch: false,
    onlyExternal: true,
  });

  $awardItem.on('click', function() {
    const $this = $(this);
    switchToItem($this);
    voteSwiper.slideTo($this.index());
  });

  /* common function */
  function switchToItem($item) {
    $item.nextAll().removeClass('preterite');
    $item.prevAll().addClass('preterite');
    $item.addClass('active').siblings().removeClass('active');
  }

  const $productMask = $('.js-product-mask');

  if (isMobileView()) {
    $productMask.click(function() {
      $(this).parents('.product-item').toggleClass('on-back').siblings().removeClass('on-back');
    })
  } else {
    $productMask.hover(function() {
      $(this).parents('.flip-wrapper').addClass('flip');
    }, function() {
      $(this).parents('.flip-wrapper').removeClass('flip');
    })
  }

  $('.js-goto-vote').click(() => scrollTo({ selector: '#vote' }));

  if (!isMobileView()) render(<ShareFixed />, document.getElementById('share-fixed'))
});
