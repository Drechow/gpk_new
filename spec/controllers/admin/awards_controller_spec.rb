require 'rails_helper'

RSpec.describe Admin::AwardsController, type: :controller do

  let(:inno) { create(:innoaward) }

  describe '#index' do
    it 'render index' do
      get :index
      expect(response).to render_template(:index)
    end
  end

  describe '#create' do
    it 'when valid' do
      expect {
        post :create, award: build_attributes(:award)
      }.to change(Award, :count).by(1)
    end
  end

  describe '#destroy' do
    let!(:award) { create(:award, innoaward: inno) }
    it 'destroy item' do
      expect {
        delete :destroy, id: award.id
      }.to change(Award, :count).by(-1)
    end
  end
end
