class InnoawardsProducts < ActiveRecord::Migration
  def change
    create_table :innoawards_products do |t|
      t.uuid :innoaward_id, null: false, index: true
      t.uuid :product_id, null: false, index: true

      t.timestamps null: false
    end
  end
end
