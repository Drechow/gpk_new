import $ from 'jquery';

let init = function () {
  if(!$('body').hasClass('products_show')) return;

  let mySwiper = new Swiper('#show-page-slider', {
    loop: true,
    pagination: '.swiper-pagination',
    nextButton: '.swiper-button-next',
    prevButton: '.swiper-button-prev'
  });
};

export default init;
