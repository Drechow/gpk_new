class ChangeProductRecommentTextToArray < ActiveRecord::Migration
  def self.up 
    change_column :products, :recommended_platform, "varchar[] USING (string_to_array(recommended_platform, ','))", default: []
  end

  def self.down
    change_column :products, :recommended_platform, "varchar USING(recommended_platform[0])"
  end
end
