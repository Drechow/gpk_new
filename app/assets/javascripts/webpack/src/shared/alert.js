import Vue from 'vue';

let alertComponent = function () {
  return new Vue({
    el: '#alert',
    data: {
      isShow: false,
      message: null,
      type: null
    },
    methods: {
      open: function ({message, type, duration}) {
        this.$data.isShow = false;
        setTimeout(() => {
          this.$data.isShow = true;
          this.$data.message = message;
          this.$data.type = `alert-${type}`;
          setTimeout(() => {
            this.$data.isShow = false;
          }, duration || 3000);
        }, 100);
      },
      close: function () {
        this.$data.isShow = false;
      }
    }
  });
};

export default alertComponent;
