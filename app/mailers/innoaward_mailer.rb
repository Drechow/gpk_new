class InnoawardMailer < ApplicationMailer
  def new_product_success product
    email = product.c_email
    mail(to: email, subject: 'innoawards报名成功提醒')
  end
end
