require 'rails_helper'

RSpec.describe Admin::ProductsController, type: :controller do
  describe '#index' do
    context 'render index' do
      it {
        get :index
        expect(response).to render_template(:index)
      }
    end
  end

  describe '#create' do
    it 'create item' do
      expect {
        post :create, product: build_attributes(:product)
      }.to change(Product, :count).by(1)
    end
  end

  describe '#show' do
    let!(:product) { create(:product) }
    context 'render show' do
      it {
        get :show, id: product.id
        expect(response).to render_template(:show)
      }
    end
  end

  describe '#destroy' do
    let!(:product) { create(:product) }
    context 'destory item' do
      it {
        count_before = Product.count
        delete :destroy, id: product.id
        expect(Product.count).to eq(count_before - 1)
      }
    end
  end
end
