import $ from 'jquery';

const header = () => {
  const $header = $('.header');
  const $meunBtn = $header.find('.js-menu-btn');
  const $nav = $header.find('.js-nav');

  $meunBtn.on('click', function() {
    $(this).toggleClass('is-show');
    $nav.toggleClass('is-show');
  });

  $nav.find('.item').on('click', function() {
    $nav.removeClass('is-show');
    $meunBtn.removeClass('is-show');
  })
}


export default header;
