class AddOrderToRanks < ActiveRecord::Migration
  def change
    add_column :ranks, :order, :integer, default: 4
  end
end
