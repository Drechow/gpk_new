const path = require('path');
const vue = require('vue-loader');
const webpack = require('webpack');
const isDev = process.env.NODE_ENV === 'dev';

var config = {
  context: __dirname,
  entry: {
    innoawards: './src/innoawards/innoawards_2015.js',
    innoawards_2016: './src/innoawards/2016/index.js',
    products: './src/innoawards/products.js',
    products_2016: './src/products/2016/index.js',
    common_2016: './src/common/2016/index.js',
  },
  output: {
    path: '../dist/',
    filename: '[name].js',
    publicPath: 'dist/'
  },
  resolve: {
    root: [
      path.join(__dirname, 'src/'),
      path.join(__dirname, 'vendor/')
    ],
    extensions: ['', '.js', '.vue'],
    alias: {}
  },
  module: {
    noParse: [],
    loaders: [
      { test: /\.js$/, loader: 'babel-loader', exclude: /node_modules/ },
      { test: /\.vue$/, loader: vue.withLoaders({ js: 'babel'}) },
      { test: require.resolve('vue'), loader: 'expose?Vue' }
    ]
  },
  plugin: [
    new webpack.optimize.UglifyJsPlugin(),
    new webpack.optimize.DedupePlugin()
  ],
  externals: {
    'jquery': 'jQuery'
  },
  devServer: {
    proxy: {
      '*': 'http://127.0.0.1:3000'
    },
    devtool: true,
    colors: true,
    progress: true,
    host: '0.0.0.0'
  }
};

module.exports = config;
