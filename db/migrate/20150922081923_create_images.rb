class CreateImages < ActiveRecord::Migration
  def change
    create_table :images do |t|
      t.string :file
      t.string :desc
      t.uuid :imageable_id, index: true
      t.string :imageable_type
      t.integer :order

      t.timestamps null: false
    end
  end
end
