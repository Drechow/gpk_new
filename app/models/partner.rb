class Partner
  require 'open-uri'

  class << self
    def all
      Rails.cache.fetch('partners') do
        get_from_remote
      end
    end

    def search query
      res = all.select do |p|
              p['name'].include?(query)
            end
      res.map do |p|
        {id: p['id'], name: p['name']}
      end
    end

    def where ids
      ids.split(',').map do |id|
        all.select{|p| p['id'] == id}.first
      end
    end

    def refresh
      Rails.cache.write('partners', get_from_remote)
    end

    def get_from_remote
      response = open('http://events.geekpark.net/api/v1/partners.json').read
      partners = JSON.parse(response)['partners']
    end
  end
end
