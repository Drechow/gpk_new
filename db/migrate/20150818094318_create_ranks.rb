class CreateRanks < ActiveRecord::Migration
  def change
    create_table :ranks, id: :uuid, default: 'uuid_generate_v4()' do |t|
      t.uuid :innoaward_id, null: false, index: true
      t.uuid :product_id, null: false, index: true
      t.uuid :award_id, null: false, index: true
      t.integer :vote, default: 0, index: true

      t.timestamps null: false
    end
  end
end
