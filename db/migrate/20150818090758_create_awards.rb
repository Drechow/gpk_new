class CreateAwards < ActiveRecord::Migration
  def change
    create_table :awards, id: :uuid, default: 'uuid_generate_v4()' do |t|
      t.string :name, null: false, index: true
      t.string :summary
      t.text :description
      t.uuid :innoaward_id, null: false, index: true

      t.timestamps null: false
    end
  end
end
