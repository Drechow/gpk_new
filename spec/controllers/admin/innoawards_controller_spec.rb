require 'rails_helper'

RSpec.describe Admin::InnoawardsController, type: :controller do
  describe '#index' do
    context 'render index' do
      it {
        get :index
        expect(response).to render_template(:index)
      }
    end
  end

  describe '#create' do
    context 'when valid' do
      it {
        count_before = Innoaward.count
        post :create, innoaward: attributes_for(:innoaward)
        expect(Innoaward.count).to eq(count_before + 1)
      }
    end
  end

  describe '#show' do
    let(:inno) { create(:innoaward) }
    context 'render show' do
      it {
        get :show, id: inno.id
        expect(response).to render_template(:show)
      }
    end
  end

  describe '#destroy' do
    let!(:inno) { create(:innoaward) }
    context 'destroy item' do
      it {
        count_before = Innoaward.count
        delete :destroy, id: inno.id
        expect(Innoaward.count).to eq(count_before - 1)
      }
    end
  end
end
