Rails.application.routes.draw do
  root 'innoawards#index'
  # resources :products, except: [:create, :new] do
  #   get 'success', action: :success, on: :collection
  # end

  resources :products, except: [:create, :new] do
    get 'success', action: :success, on: :collection
  end

  # 投票
  # resources :ranks, only: :index
  resources :ranks, only: :index do
    post :vote
  end

  # 主站 oauth 登陆
  get 'users/auth/geekpark' => 'sessions#omniauth', as: :login
  match 'users/auth/:provider/callback' => 'sessions#omniauth', via: [:get, :post]
  delete 'sessions/sign_out', as: :logout

  namespace :admin do
    root 'innoawards#index'

    resources :product_as do
      patch 'add_screenshot', on: :member
      get 'search', on: :collection
    end

    resources :ranks, only: [:new, :create, :update, :destroy]

    # 后台管理 创新产品评选
    resources :innoawards do
      get 'page/:page', action: :index, on: :collection
      resources :awards do
        patch 'change_order', on: :member
      end
      get 'ranks', on: :member
      get 'partners', on: :member
      post 'partners', on: :member, to: 'innoawards#update_partners'

      resources :images, only: [:create, :update, :destroy]
      resources :products do
        get 'page/:page', action: :index, on: :collection
        get 'search', action: :search, on: :collection
        patch 'approve', on: :member
      end
    end

    resources :news

    resources :images, only: [:destroy] do
      post 'create_image', on: :collection
    end

    get 'partners/refresh' => 'partners#refresh'
  end

  get '/innoawards', to: 'innoawards#index'

  get 'upbrowser', to: 'innoawards#upbrowser'

  # mount api
  mount V1::API => '/api'
  mount GrapeSwaggerRails::Engine => 'api_doc'

end
