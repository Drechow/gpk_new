# This file is used by Rack-based servers to start the application.

require ::File.expand_path('../config/environment', __FILE__)

require 'grape/jbuilder'

use Rack::Config do |env|
  env['api.tilt.root'] = 'app/api/v1/jbuilders'
end

require 'rack/cors'

use Rack::Cors do
  allow do
    origins '*'
    resource '*', headers: :any, methods: [:get, :post]
  end
end

run Rails.application
