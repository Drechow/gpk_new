require 'capistrano-db-tasks'
# config valid only for Capistrano 3.1
lock '3.4.0'

set :application, 'gpk_new'
set :repo_url, 'git@github.com:geekpark/gpk_new.git'

# Default branch is :master
# ask :branch, proc { `git rev-parse --abbrev-ref HEAD`.chomp }.call

set :use_sudo, false
set :bundle_binstubs, nil

# Default deploy_to directory is /var/www/my_app
set :deploy_to, '/home/www/gpk_new_production'

set :shared_path, "#{deploy_to}/shared"

# rbenv ruby
set :rbenv_ruby, '2.2.3'

# unicorn config path
set :unicorn_config_path, '/home/www/gpk_new_production/current/config/unicorn.rb'

# Default value for :scm is :git
# set :scm, :git

# Default value for :format is :pretty
# set :format, :pretty

# Default value for :log_level is :debug
# set :log_level, :debug

# Default value for :pty is false
set :pty, true

# Default value for :linked_files is []
set :linked_files, %w{config/database.yml config/secrets.yml .rbenv-vars config/initializers/omniauth.rb .env}

# Default value for linked_dirs is []
set :linked_dirs, %w{log tmp/pids tmp/cache tmp/sockets vendor/bundle public/system public/uploads}

# Default value for default_env is {}
# set :default_env, { path: "/opt/ruby/bin:$PATH" }

# Default value for keep_releases is 5
# set :keep_releases, 5

# capistrano-db-tasks start

# if you want to remove the local dump file after loading
set :db_local_clean, true

# if you want to remove the dump file from the server after downloading
set :db_remote_clean, true

# if you want to exclude table from dump
set :db_ignore_tables, []

# if you want to exclude table data (but not table schema) from dump
set :db_ignore_data_tables, []

# If you want to import assets, you can change default asset dir (default = system)
# This directory must be in your shared directory on the server
set :assets_dir, 'public/uploads'
set :local_assets_dir, 'public/uploads'

# if you want to work on a specific local environment (default = ENV['RAILS_ENV'] || 'development')
# set :locals_rails_env, "production"

# if you are highly paranoid and want to prevent any push operation to the server
set :disallow_pushing, true

# if you prefer bzip2/unbzip2 instead of gzip
set :compressor, :bzip2

# capistrano-db-tasks end

namespace :deploy do

  desc 'Commands for unicorn application'
  %w(start stop force-stop restart upgrade reopen-logs).each do |command|
    task command.to_sym do
      on roles(:app), in: :sequence, wait: 5 do
        execute "/etc/init.d/unicorn_gpk_new_production #{command}"
      end
    end
  end

  # compile assets locally then rsync
  after :finishing, 'deploy:cleanup'

  after :publishing, :restart

end
