class Admin::PartnersController < ApplicationController

  def search
    respond_to do |format|
      format.json { render json: Partner.search(params[:query])}
    end
  end

  def refresh
    respond_to do |format|
      if Partner.refresh
        format.json { render json: {result: 'success'} }
      else
        format.json { render json: {result: 'error'} }
      end
    end
  end
end
