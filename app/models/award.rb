class Award < ActiveRecord::Base
  include RankedModel

  validates :name, presence: true

  has_many :products, through: :ranks
  has_many :ranks, dependent: :destroy
  belongs_to :innoaward

  ranks :row_order, with_same: :innoaward_id

  delegate :year, to: :innoaward

  def winers
    self.ranks.where(order: 1).map { |rank| rank.product }
  end
end
