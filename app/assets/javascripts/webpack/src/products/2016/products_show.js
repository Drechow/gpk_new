import $ from 'jquery';
import { isMobileView, isMobile } from 'mdetect';

const init = function () {
  if(!$('body').hasClass('products_show')) return;

  const isApp = $('#screenshots-container').data('category') === 'app';
  let count = 1;
  if (isApp) {
    if (isMobileView()) {
      count = 2
    } else {
      count = 3;
    };
  }

  const $productWrapper = $('.js-product-wrapper')

  $productWrapper.each(function(item) {
    const $this = $(this);
    const $productDes = $this.find('.product-des');

    const height = $productDes.height();
    if (height > 176) {
      $this.addClass('show-pucker');
      $productDes.css('height', 176);
    };

    $this.find('.unfold-btn').on('click', e => {
      e.preventDefault();
      $this.toggleClass('unfold');

      if ($this.hasClass('unfold')) {
        $productDes.animate({ 'height': height }, 200);
      } else {
        $productDes.animate({ 'height': 176 }, 200);
      }
    });

  });


  const screenshotsSwiper = new Swiper('#screenshots-container', {
    loop: true,
    slidesPerView : count,
    pagination: '.screenshots-pagination',
    paginationClickable: true,
    nextButton: '.screenshots-button-next',
    prevButton: '.screenshots-button-prev',
  });

  const otherSwiper = new Swiper('#other-container', {
    loop: true,
    slidesPerView : isMobileView() ? 2 : 4,
    slidesPerGroup : isMobileView() ? 2 : 3,
    nextButton: '.other-button-next',
    prevButton: '.other-button-prev',
  })
};

export default init;
