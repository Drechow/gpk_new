import $ from 'jquery';
import Vue from 'vue';
import navSync from '../../vendor/navsync';
import goTop from '../shared/go_top';
import scrollTo from '@geekpark/scroll-to';
import vote from './vote';
import { isWechat } from '@geekpark/ismobile';

$(function () {
  goTop();
  if(!isWechat()) {
    $('.visible-wechat').hide();
  }
});

$(function () {
  if(!$('body').hasClass('innoawards_index')) return;

  // init historyReview section
  let historyReview  = new Vue({
    el: '#history-review',
    data: {
      current: 2014
    },
    methods: {
      changePanel: function (year) {
        this.$data.current = year;
      }
    }
  });

  // initial vote section
  vote.init();

  $('nav.nav').navSync({
    highlightClass: 'active'
  });

  $('#go-apply').on('click', function (e) {
    e.preventDefault();
    scrollTo({
      selector: $(this).attr('href'),
      offset: 55,
      callback() {
        ga('send', 'event', 'vote', 'go_vote', 'slide to vote');
      }
    });
  });
});
