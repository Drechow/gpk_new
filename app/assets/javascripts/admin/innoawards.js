$(function() {
  if (!$('body').hasClass('admin-innoawards')) return;

  // refresh partner list
  $('#refresh-partner').on('click', function(e){
    var res = $.get('/admin/partners/refresh', function(e){
      if (e.result === 'success') {
        alert('更新合作伙伴列表成功');
      } else {
        alert('更新合作伙伴列表失败，请联系管理员。')
      }
    });
  });

  function mountDropdown() {
    $('.ui.dropdown').dropdown();
  }

  if($('body').hasClass('admin-innoawards-ranks')){
    var updateOrder = $('.update-order');
    updateOrder.on('change', function(){
      var $this = $(this);
      $.ajax({
        type: 'PATCH',
        url: $this.data('url'),
        data: {rank: {order: $this.val()}}
      }).then(function(data) {
        location.reload();
      }, function() {
        alert('更新失败');
      });
   });
  };

  if($('body').hasClass('admin-innoawards-partners')) return;
// images upload
  var uploadData = new Vue({
    el: '#previews',
    data: {
      picItems: gon.innoaward ? _.map(gon.innoaward.header_images, 'url') : []
    },
    methods: {
      addPreviews: function(e) {
        e.stopPropagation();
        uploadData.picItems.push({des: ''});
        var index = uploadData.picItems.length;
        var _this = e.currentTarget;
        Vue.nextTick(function() {
          var $dom = $(_this).siblings('.pic-item').find('.upload-input').eq(index - 1);
          console.log($dom);
          $dom.trigger('click');
        });
      },
      removePreviews: function(index) {
        uploadData.picItems.$remove(index);
      },
      triggerInput: function(e){
        e.preventDefault();
        var _this = e.target;
        $(_this).parents('.pic-item').find('.upload-input').eq(0).trigger('click');
      },
      showThumb: function(event){
        var _this = event.target;
        if(_this.files[0]){
          var reader = new FileReader();
          reader.onload = function(e) {
            $(_this).siblings('.image-preview').find('img').attr('src', e.target.result);
          }
          reader.readAsDataURL(_this.files[0]);
        }else{
          $(_this).siblings('.image-preview').find('img').attr('src', 'https://www.launchbit.com/taz-i/6105-graphicstockcamera.jpg');
        }
      }
    }
  });



  $('#upload-image').on('click', function(){
    $('#image-input').trigger('click');
  });

  $('#image-input').on('change', function(){
    if($(this).val() !== ''){
      var formData = new FormData();
      formData.append('file', $('#image-input').get(0).files[0]);
      $.ajax({
        type: 'POST',
        url: $('#upload-image').data('href'),
        data: formData,
        cache: false,
        processData: false,
        contentType: false
      }).then(function(data) {
        console.log('success');
        location.reload();
      }, function() {
        console.log('error');
      });
    }
  });

  $('.desc-save-btn').on('click', function(){
    var descData = {'desc': $(this).siblings('.image-desc').val()};
    $.ajax({
      type: 'PATCH',
      url: $(this).data('href'),
      data: descData
    }).then(function(){
      console.log('success');
      location.reload();
    }, function(){
      console.log('error');
    });
  });
});
