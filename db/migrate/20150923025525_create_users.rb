class CreateUsers < ActiveRecord::Migration
  def change
    create_table :users, id: :uuid do |t|
      t.string :email, index: true
      t.string :username, index: true
      t.string :avatar
      t.string :token
      t.string :refresh_token
      t.datetime :expires_at

      t.string :remember_token
      t.datetime :remember_token_created_at

      t.string :role

      t.jsonb :user_info, default: {}

      t.timestamps null: false
    end
  end
end
