FactoryGirl.define do
  factory :innoaward, class: Innoaward do
    year 2015
    title "2015 中国互联网创新产品评选"
    summary ""
    description ""
    schedule ""
    extra_info ""

    trait :next_year do
      year 2016
      title "2016 中国互联网创新产品评选"
    end
  end
end
