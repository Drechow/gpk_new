module ApplicationHelper
  def absolute_url(url)
    unless url[/\Ahttp:\/\//] || url[/\Ahttps:\/\//]
      url = "http://#{url}"
    end
    url
  end

  def hash_absolute_url(anchor)
    unless controller_name == 'innoawards' && action_name == 'index'
      anchor = innoawards_path + anchor
    end
    anchor
  end
end
