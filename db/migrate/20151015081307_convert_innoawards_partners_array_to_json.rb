class ConvertInnoawardsPartnersArrayToJson < ActiveRecord::Migration
  def change
    remove_column :innoawards, :partners, :jsonb, array: true
    add_column :innoawards, :partners, :jsonb, default: {categories: '', detail: {}}
  end
end
