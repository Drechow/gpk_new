class Innoaward < ActiveRecord::Base
  include Newsable

  paginates_per 10

  validates :title, :year, presence: true
  validates :year, uniqueness: true

  has_many :awards
  has_many :ranks
  has_many :products, through: :innoawards_products
  has_many :innoawards_products
  has_many :header_images, class_name: 'Image', as: :imageable, dependent: :destroy

  def get_ranks
    return self.awards.map {|x| {
      info: x,
      ranks: x.ranks.order(order: :asc, created_at: :desc).map {|y| {
        rank: y,
        product: y.product
      }}
    }}
  end

  def get_partners_json
    partners['categories'].split(',').map do |cat|
      {'category' => cat, 'detail' => Partner.where(partners['detail'][cat])}
    end
  end
end
