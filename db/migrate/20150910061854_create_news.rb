class CreateNews < ActiveRecord::Migration
  def change
    create_table :news do |t|
      t.string :title, null: false, index: true
      t.string :url
      t.string :cover

      # polymorphic
      t.uuid :newsable_id
      t.string :newsable_type

      t.timestamps null: false
    end
  end
end
