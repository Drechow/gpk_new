# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20161017092546) do

  # These are extensions that must be enabled in order to support this database
  enable_extension "plpgsql"
  enable_extension "uuid-ossp"

  create_table "awards", id: :uuid, default: "uuid_generate_v4()", force: :cascade do |t|
    t.string   "name",         null: false
    t.string   "summary"
    t.text     "description"
    t.uuid     "innoaward_id", null: false
    t.datetime "created_at",   null: false
    t.datetime "updated_at",   null: false
    t.integer  "row_order"
  end

  add_index "awards", ["innoaward_id"], name: "index_awards_on_innoaward_id", using: :btree
  add_index "awards", ["name"], name: "index_awards_on_name", using: :btree

  create_table "images", force: :cascade do |t|
    t.string   "file"
    t.string   "desc"
    t.uuid     "imageable_id"
    t.string   "imageable_type"
    t.integer  "order"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "images", ["imageable_id"], name: "index_images_on_imageable_id", using: :btree

  create_table "innoawards", id: :uuid, default: "uuid_generate_v4()", force: :cascade do |t|
    t.integer  "year",                                                  null: false
    t.string   "title",                                                 null: false
    t.text     "about"
    t.text     "rule"
    t.jsonb    "schedule",   default: {}
    t.jsonb    "extra_info", default: {}
    t.datetime "created_at",                                            null: false
    t.datetime "updated_at",                                            null: false
    t.jsonb    "partners",   default: {"detail"=>{}, "categories"=>""}
  end

  add_index "innoawards", ["title"], name: "index_innoawards_on_title", using: :btree
  add_index "innoawards", ["year"], name: "index_innoawards_on_year", using: :btree

  create_table "innoawards_products", force: :cascade do |t|
    t.uuid     "innoaward_id", null: false
    t.uuid     "product_id",   null: false
    t.datetime "created_at",   null: false
    t.datetime "updated_at",   null: false
  end

  add_index "innoawards_products", ["innoaward_id"], name: "index_innoawards_products_on_innoaward_id", using: :btree
  add_index "innoawards_products", ["product_id"], name: "index_innoawards_products_on_product_id", using: :btree

  create_table "news", force: :cascade do |t|
    t.string   "title",         null: false
    t.string   "url"
    t.string   "cover"
    t.uuid     "newsable_id"
    t.string   "newsable_type"
    t.datetime "created_at",    null: false
    t.datetime "updated_at",    null: false
  end

  add_index "news", ["title"], name: "index_news_on_title", using: :btree

  create_table "product_as", id: :uuid, default: "uuid_generate_v4()", force: :cascade do |t|
    t.string   "logo"
    t.string   "name",            null: false
    t.string   "summary"
    t.text     "description"
    t.date     "published_at"
    t.string   "url"
    t.string   "category"
    t.uuid     "user_id"
    t.jsonb    "contacts"
    t.jsonb    "founders"
    t.jsonb    "financings"
    t.jsonb    "cowork_records"
    t.jsonb    "editor_comments"
    t.uuid     "innoaward_id"
    t.datetime "created_at",      null: false
    t.datetime "updated_at",      null: false
  end

  add_index "product_as", ["category"], name: "index_product_as_on_category", using: :btree
  add_index "product_as", ["name"], name: "index_product_as_on_name", using: :btree
  add_index "product_as", ["user_id"], name: "index_product_as_on_user_id", using: :btree

  create_table "products", id: :uuid, default: "uuid_generate_v4()", force: :cascade do |t|
    t.string   "logo"
    t.string   "screenshots",                                    array: true
    t.string   "name",                              null: false
    t.string   "summary"
    t.text     "description"
    t.date     "published_at"
    t.string   "url"
    t.string   "weibo"
    t.string   "wechat"
    t.string   "category"
    t.string   "recommended_platform", default: [],              array: true
    t.string   "c_name",                            null: false
    t.string   "c_corp"
    t.string   "c_position"
    t.string   "c_email"
    t.string   "c_mobile"
    t.string   "c_corp_addr"
    t.text     "evaluation"
    t.string   "status",                            null: false
    t.datetime "created_at",                        null: false
    t.datetime "updated_at",                        null: false
    t.string   "note"
  end

  add_index "products", ["c_name"], name: "index_products_on_c_name", using: :btree
  add_index "products", ["category"], name: "index_products_on_category", using: :btree
  add_index "products", ["name"], name: "index_products_on_name", using: :btree
  add_index "products", ["recommended_platform"], name: "index_products_on_recommended_platform", using: :btree
  add_index "products", ["status"], name: "index_products_on_status", using: :btree

  create_table "ranks", id: :uuid, default: "uuid_generate_v4()", force: :cascade do |t|
    t.uuid     "innoaward_id",             null: false
    t.uuid     "product_id",               null: false
    t.uuid     "award_id",                 null: false
    t.integer  "vote",         default: 0
    t.datetime "created_at",               null: false
    t.datetime "updated_at",               null: false
    t.integer  "order",        default: 4
  end

  add_index "ranks", ["award_id"], name: "index_ranks_on_award_id", using: :btree
  add_index "ranks", ["innoaward_id"], name: "index_ranks_on_innoaward_id", using: :btree
  add_index "ranks", ["product_id"], name: "index_ranks_on_product_id", using: :btree
  add_index "ranks", ["vote"], name: "index_ranks_on_vote", using: :btree

  create_table "users", id: :uuid, default: "uuid_generate_v4()", force: :cascade do |t|
    t.string   "email"
    t.string   "username"
    t.string   "avatar"
    t.string   "token"
    t.string   "refresh_token"
    t.datetime "expires_at"
    t.string   "remember_token"
    t.datetime "remember_token_created_at"
    t.string   "role"
    t.jsonb    "user_info",                 default: {}
    t.datetime "created_at",                             null: false
    t.datetime "updated_at",                             null: false
  end

  add_index "users", ["email"], name: "index_users_on_email", using: :btree
  add_index "users", ["username"], name: "index_users_on_username", using: :btree

end
