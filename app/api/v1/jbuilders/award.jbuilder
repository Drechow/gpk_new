json.awards @awards do |award|
  json.id award.id
  json.name award.name
  json.summary award.summary
  json.description award.description
  json.ranks award.ranks.order('ranks.order').includes(:product) do |rank|
    json.name rank.product.name
    json.logo rank.product.logo_url
    json.summary rank.product.summary
    json.description rank.product.description
    json.screenshots do
      json.array! rank.product.screenshots.map(&:url)
    end
  end
end
