class CreateInnoawards < ActiveRecord::Migration
  def change
    create_table :innoawards, id: :uuid, default: 'uuid_generate_v4()' do |t|
      # 基本字段
      t.integer :year, null: false, index: true
      t.string :title, null: false, index: true
      t.text :about # 关于活动
      t.text :rule # 评选规则

      # 日程
      t.jsonb :schedule, default: {}

      # 合作伙伴 ids
      t.jsonb :partners, array: true, default:  []

      # 其它备用
      t.jsonb :extra_info, default: {}

      t.timestamps null: false
    end
  end
end
