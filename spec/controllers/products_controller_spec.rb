require 'rails_helper'

RSpec.describe ProductsController, type: :controller do
  describe '#index' do
    it 'render index' do
      get :index
      expect(response).to render_template(:index)
    end
  end

  describe '#create' do
    it 'create item' do
      expect do
        post :create, product: build_attributes(:product)
      end.to change(Product, :count).by(1)
    end
  end

  describe '#destroy' do
    let!(:product) { create(:product) }
    it 'destroy item' do
      expect do
        delete :destroy, id: product.id
      end.to change(Product, :count).by(-1)
    end
  end
end
