require 'rails_helper'

RSpec.describe Admin::PartnersController, type: :controller do

  describe "GET #refresh" do
    it "returns http success" do
      get :refresh
      expect(response).to have_http_status(:success)
    end
  end

end
